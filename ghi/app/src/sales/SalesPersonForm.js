import React, { Component } from 'react'

export default class SalesPersonForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            employee_no: "",
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNoChange = this.handleEmployeeNoChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        try {
            const resp = await fetch("http://localhost:8090/api/sales_people/");
            if (resp.ok) {
                const data = await resp.json();
                this.setState({
                     // THIS MIGHT BE WRONG...CHECK W/TEAM MATE
                    sales_person: data.sales_person
                })
            }
            else {
                console.log("Location resp error!")
            }
        }
        catch(e) {
            console.log("fetch error:", e)
        }
    }

    handleNameChange(event) {
        this.setState({name: event.target.value})
    }

    handleEmployeeNoChange(event) {
        this.setState({employee_no: event.target.value})
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        delete this.state.sales_person;
        const postUrl = "http://localhost:8090/api/sales_people/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(this.state),
            headers: {"Content-Type": "application/json"}
        }
        try {
            const resp = await fetch(postUrl, fetchConfig);
            if (resp.ok) {
                console.log("handleSubmit Working...")
                this.setState({
                    name: '',
                    employee_no: '',
                })
            }
        }
        catch(e) {
            console.log("post fetch error:", e)
        }
    }
    
    render() {
        return(
            <div className='container my-5 col-6 mt-3 bg shadow p-3'>
                <h2>Add a Salesperson</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className='form-group pb-3'>
                    <label htmlFor='name'><b>Name</b></label>
                    <input onChange={this.handleNameChange} type="text" className="form-control" id='name' placeholder="name" name="name" />
                    <label htmlFor='employee_no'><b>Employee No.</b></label>
                    <input onChange={this.handleEmployeeNoChange} type="text" className="form-control" id='employee_no' placeholder="employee number" name="employee_no" />
                    </div>
                    <div className='d-flex justify-content-center'>
                    <button className='btn btn-danger'>Add</button>
                    </div>
                </form>
            </div>
        )
    }
}