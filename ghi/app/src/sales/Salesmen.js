import React from 'react'
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';




export default function Salesmen() {
    const [salesmen, setSalesmen] = useState([]);
    const [searchParam, setSearchParam] = useState('');
    useEffect(() => {
        const salesListUrl = "http://localhost:8090/api/sales_people/";
        fetch(salesListUrl)
            .then(resp => resp.json())
            .then(data => setSalesmen(data.sales_person))
    }, [])


    return (
        <div className='container'>
            <div className='d-grid justify-content-center'>
            <div className='fs-2 fw-bold'>Sales Associates:</div>
            <Link className='shadow btn btn-danger' to="/salesperson/new">Add Associate</Link>
            <div className='mb-3 mt-2 d-flex justify-content-center'>
                <input onChange={e => setSearchParam(e.target.value)} type="text" placeholder="Search Associate"/>
            </div>
            </div>
            <table className="shadow table table-hover table-striped table-bordered text-center align-middle">
                <thead>
                    <tr>
                        <th scope="col"><b>Name</b></th>
                        <th scope="col"><b>Employee Number:</b></th>
                    </tr>
                </thead>
                <tbody className='table-warning'>
                    {salesmen.filter(associate => { if (associate.name.toLowerCase().includes(searchParam.toLowerCase())) return associate }).map(associate => <tr key={associate.employee_no}><td>{associate.name}</td><td>{associate.employee_no}</td></tr>)}
                </tbody>
            </table>
        </div>
    )
}
