import React, { Component } from 'react'

export default class PotentialCustomerForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            customer_address: '',
            phone_no: '',
        }
        // bind changes go here
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handlePhoneNoChange = this.handlePhoneNoChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        try{
            const resp= await fetch("http://localhost:8090/api/potential_customers/")
            if (resp.ok) {
                const data = await resp.json();
                this.setState({
                    // THIS MIGHT BE WRONG...CHECK W/TEAM MATE
                    potential_customer: data.potential_customer
                })
            }
            else{
                console.log("Location resp error!")
            }
        }
        catch(e) {
            console.log("fetch error:", e)
        }
    }

    // handle changes go here

    handleNameChange(event) {
        this.setState({name: event.target.value})
    }

    handleAddressChange(event) {
        this.setState({customer_address: event.target.value})
    }

    handlePhoneNoChange(event) {
        this.setState({phone_no: event.target.value})
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        delete this.state.potential_customer;
        const postUrl = 'http://localhost:8090/api/potential_customers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(this.state),
            headers: {"Content-Type": "application/json"}
        }
        try {
            const resp = await fetch(postUrl, fetchConfig);
            if (resp.ok) {
                console.log("handleSubmit Working...")
                this.setState({
                    name: '',
                    customer_address: '',
                    phone_no: '',
                })
            }
        }
        catch(e) {
            console.log("post fetch error:", e)
        }
    }

    render() {
        return(
            <div className='container my-5 col-6 mt-3 bg shadow p-3'>
            <h2>Add a Customer</h2>
            <form onSubmit={this.handleSubmit}>
                <div className='form-group pb-3'>
                <label htmlFor='name'><b>Name</b></label>
                <input onChange={this.handleNameChange} type="text" className="form-control" id='name' placeholder="name" name="name" />
                <label htmlFor='employee_no'><b>Address</b></label>
                <input onChange={this.handleAddressChange} type="text" className="form-control" id='customer_address' placeholder="address" name="customer_address" />
                <label htmlFor='phone_no'><b>Phone Number</b></label>
                <input onChange={this.handlePhoneNoChange} type="text" className="form-control" id='phone_no' placeholder="phone number" name="phone_no" />
                </div>
                <div className='d-flex justify-content-center'>
                <button className='btn btn-danger'>Add</button>
                </div>
            </form>
        </div>
        )
    }
}