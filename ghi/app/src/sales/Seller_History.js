import React from 'react';
import { useState, useEffect } from 'react';

export default function SellerHistory() {
    const [sales_person, setModels] = useState([]);
    const [chosen_one, setChosenOne] = useState('');
    const [sales, setSaleList] = useState([]);
    useEffect(() => {
        fetch("http://localhost:8090/api/sales_people/")
        .then(res => res.json())
        .then(data => {setModels(data.sales_person);
        })
    }, [])


    // handlers
    function handleName(e) {
        setChosenOne(e.target.value)
    }

    // using useEffect to filter/fetch
    useEffect(()=>{
        fetch("http://localhost:8090/api/sale_records/")
        .then(res2 => res2.json())
        .then(data2 => {setSaleList(data2.sales)
        })
    }, [])

    return(
        <div className='container my-5'>
        <div className='d-grid justify-content-center'>
        <div className='fs-2 fw-bold'>Sales History</div>
        </div>
        <select onChange={handleName} className="form-select m-2">
            <option >Chose an employee...</option>
            {sales_person.map(l => <option value={l.id} key={l.id}>{l.name}</option>)}
        </select>
        {/* USE THE BELOW DIV TO CUSTOMIZE TABLE AESTHETICS */}
        <div>
        <table className='shadow table table-hover table-striped table-bordered text-center align-middle'>
            <thead>
                <tr>
                    <th scope='col'>Sales Person</th>
                    <th scope='col'>Customer</th>
                    <th scope='col'>Automobile VIN</th>
                    <th scope='col'>Sale Price</th>
                </tr>
            </thead>
            <tbody className='table-warning'>
                {sales.filter(p => {return (
                p.sales_person.id == chosen_one)}).map(e => {return (
                    <tr key={e.automobile.vin}>
                    <th>{e.sales_person.name}</th>
                    <th>{e.customer.name}</th>
                    <th>{e.automobile.vin}</th>
                    <th>{`$${e.sale_price}.00`}</th>
                    </tr>
                )})}
            </tbody>
        </table>
        </div>
        </div>
    )
    
}



