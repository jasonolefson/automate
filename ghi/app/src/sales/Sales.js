import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Sales() {
    const [sales, setModels] = useState([]);
    useEffect(() => {
        fetch("http://localhost:8090/api/sale_records/")
        .then(res => res.json())
        .then(data => {setModels(data.sales);
        console.log(data)})
    }, [])

    return(
        <div className='container text-center my-3'>
            <div className='d-grid justify-content-center'>
            <div className='fs-2 fw-bold'>Sales</div>
            <Link className="shadow btn btn-danger mb-3" to="/sales/new">Add Sale</Link>
            </div>
            <table className='shadow table table-hover table-striped table-bordered text-center align-middle'>
                <thead>
                    <tr>
                        <th scope='col'>Seller Name</th>
                        <th scope='col'>Employee Number</th>
                        <th scope='col'>Purchaser Name</th>
                        <th scope='col'>Automobile VIN</th>
                        <th scope='col'>Price</th>
                    </tr>
                </thead>
                <tbody className='table-warning'>
                {sales.map(e => {return (
                <tr key={e.automobile.vin}>
                    <th>{e.sales_person.name}</th>
                    <th>{e.sales_person.employee_no}</th>
                    <th>{e.customer.name}</th>
                    <th>{e.automobile.vin}</th>
                    <th>{`$${e.sale_price}.00`}</th>
                </tr>
                )})}
                </tbody>
            </table>
        </div>
    )
}
