import React, {Component} from 'react'

export default class SalesForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            automobiles: [],
            salespeople: [],
            customers: [],
            sales_person: "",
            customer: "",
            automobile: "",
            sale_price: "",
        }
        this.handleSalesPersonNameChange = this.handleSalesPersonNameChange.bind(this);
        this.handleCustomerNameChange = this.handleCustomerNameChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        // Mount for vehicle list
        try {
            const resp = await fetch("http://localhost:8090/api/sellable_cars/");
            if (resp.ok) {
                const data = await resp.json();
                this.setState({
                    automobiles: data.vehicle
                })
            }
            else {
                console.log("Location resp error!")
            }
        }
        catch(e) {
            console.log("Fetch vehicleData error:", e)
        }

        // Mount for customer list
        try{
            const resp = await fetch("http://localhost:8090/api/potential_customers/");
            if (resp.ok) {
                const customerData = await resp.json();
                this.setState({
                    customers: customerData.potential_customer
                })
        }
            else {
                console.log("Location resp2 error!")
            }
        }
        catch(f) {
            console.log("Fetch customerData error:", f)
        }

        // Mount for salesperson list
        try{
            const resp = await fetch("http://localhost:8090/api/sales_people/");
            if (resp.ok) {
                const salespersonData = await resp.json();
                this.setState({
                    salespeople: salespersonData.sales_person
                })
        }
            else {
                console.log("Location resp3 error!")
            }
        }
        catch(f) {
            console.log("Fetch salespersonData error:", f)
        }

    }

    // does this work with a dropdown menu?
    handleAutomobileChange (event) {
        this.setState({automobile: event.target.value})
    }

    handleSalesPersonNameChange(event) {
        this.setState({sales_person: event.target.value})
    }

    handleCustomerNameChange(event) {
        this.setState({customer: event.target.value})
    }

    handlePriceChange(event) {
        this.setState({sale_price: event.target.value})
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        delete this.state.salespeople;
        delete this.state.automobiles;
        delete this.state.customers;
        const postUrl = "http://localhost:8090/api/sale_records/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(this.state),
            headers: {"Content-Type": "application/json"}
        }
        const putUrl = ""
        try {
            const resp = await fetch(postUrl, fetchConfig);
            console.log(resp)
            if (resp.ok) {
                console.log("HandleSubmit Working...")
                // this.setState({
                //     sales_person: "",
                //     customer: "",
                //     sales_price: "",
                // })
            }
        }
        catch(e) {
            console.log("post fetch error:", e)
        }
        return window.location.href='/sales'
    }


    render() {
        return(
            <div className='container my-5 col-6 mt-3 bg shadow p-3'>
            <h2>Add a Sale</h2>
            <form onSubmit={this.handleSubmit}>
                <div className='form-group pb-3'>
                    {/* Salesperson Name Dropdown */}
                    <label htmlFor='sales_person'><b>Employee Name</b></label>
                    <select name="sales_person" onChange={this.handleSalesPersonNameChange} className="form-select" id="sales_person">
                        <option value=''>choose a salesperson...</option>
                         {/* map start */}
                         {this.state.salespeople.map(emp => <option value={emp.id} key={emp.id}>{`${emp.name}`}</option>)}
                         {/* map end */}
                    </select>
                    {/* Customer Name Dropdown */}
                    <label htmlFor='customer'><b>Customer Name</b></label>
                    <select name="customer" onChange={this.handleCustomerNameChange} className="form-select" id="customer">
                        <option value=''>choose a customer...</option>
                         {/* map start */}
                         {this.state.customers.map(cust => <option value={cust.id} key={cust.id}>{`${cust.name}`}</option>)}
                         {/* map end */}
                    </select>
                    {/* dropdown menu with AVAILABLE cars START */}
                    <label htmlFor='automobile'><b>Vehicle</b></label>
                    <select name="automobile" onChange={this.handleAutomobileChange} className="form-select" id="automobile">
                        <option value=''>choose a vehicle...</option>
                         {/* map start */}
                         {this.state.automobiles.map(l => <option value={l.import_href} key={l.import_href}>{`${l.vin}`}</option>)}
                         {/* .filter(sellable => {return (sellable.sold == false)}) */}
                         
                         {/* map end */}
                    </select>
                    {/* dropdown menu with AVAILABLE cars END */}
                    <label htmlFor='sale_price'><b>Price</b></label>
                    <input onChange={this.handlePriceChange} type="text" className="form-control" id='sale_price' placeholder="sale price" name="sale_price" />
                </div>
                <div className='d-flex justify-content-center'>
                <button type="submit" className='btn btn-danger'>Add</button>
                </div>
            </form>
            </div>
        )
    }
}