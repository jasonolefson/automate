import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';

export default function VehicleModels() {
    const [models, setModels] = useState([]);
    useEffect(() => {
        fetch("http://localhost:8100/api/models")
            .then(res => res.json())
            .then(data => {
                setModels(data.models);
            })
    }, [])


    return (
        <div>
            <div className=''></div>
            <h1 className='text-center'>Vehicle Models</h1>
            <div className='d-flex justify-content-center'>
            <Link to="/vehiclemodels/new" className='shadow col-2 btn btn-danger mb-3 shadow'>Add Vehicle</Link>
            </div>
            <table className="shadow table table-hover table-striped table-bordered text-center align-middle">
                <thead>
                    <tr>
                        <th scope="col"><b>Name</b></th>
                        <th scope="col"><b>Manufacturer</b></th>
                        <th scope='col'><b>Picture</b></th>
                    </tr>
                </thead>
                <tbody className='table-warning'>
                    {models.map(e => {
                        return (
                            <tr key={e.name}>
                                <th>{e.name}</th>
                                <th>{e.manufacturer.name}</th>
                                <th>
                                    <img alt='carImage' style={{ maxWidth: "260px", maxHeight: "160px" }} src={e.picture_url} />
                                </th>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
