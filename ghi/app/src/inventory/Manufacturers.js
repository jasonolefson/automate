import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Manufacturers() {
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(res => res.json())
            .then(data => setManufacturers(data.manufacturers))
    }, [])

    return (
        <div className='container col-4 text-center'>
            <Link className='btn btn-danger mt-4 mb-3 shadow' to="/manufacturers/new">Add Manufacturer</Link>
            <table className="shadow table table-hover table-striped table-bordered text-center align-middle">
                <thead>
                    <tr>
                        <th className="fs-4" scope="col">Manufacturer List</th>
                    </tr>
                </thead>
                <tbody class="shadow table-warning">
                    {manufacturers.map(e => { return (<tr key={e.name}><th>{e.name}</th></tr>) })}
                </tbody>
            </table>
        </div>
    )
}
