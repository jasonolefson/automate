import React, { Component } from 'react'

export default class VehicleModelForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            manufacturer_id: '',
            manufacturers: [],
            picture_url: '',
        }

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePicChange = this.handlePicChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount() {
        try {
            const resp = await fetch("http://localhost:8100/api/manufacturers/");
            if (resp.ok) {
                const data = await resp.json();
                this.setState({
                    manufacturers: data.manufacturers
                })
            }
            else {
                console.log("location resp error!")
            }
        }
        catch (e) {
            console.log("fetch error:", e)
        }
    }

    handleNameChange(event) {
        this.setState({name: event.target.value})
    }

    handlePicChange(event) {
        this.setState({picture_url: event.target.value})
    }

    handleManufacturerChange(event) {
        this.setState({manufacturer_id: event.target.value})
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        delete this.state.manufacturers;
        const postUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(this.state),
            headers: {"Content-Type": "application/json"}
        }
        try {
            const resp = await fetch(postUrl, fetchConfig);
            if (resp.ok) {
                console.log("what's up")
                this.setState({
                    name: "",
                    manufacturer_id: '',
                    manufacturers: [],
                    picture_url: '',
                })
            }
        }
        catch (e) {
            console.log("post fetch error", e)
        }
    }




    render() {
    return (
        <div className='container my-5 shadow p-3'>
            <form onSubmit={this.handleSubmit}>
                <div className='form-group pb-3'>
                    <label htmlFor='new_model'><b>Model Name</b></label>
                    <input onChange={this.handleNameChange} type="text" className="form-control" placeholder='enter new model name'/>
                    <label htmlFor='manufacturer'><b>Picture URL</b></label>
                    <input onChange={this.handlePicChange} type="text" className="form-control" placeholder='enter picture url'/>
                    <label htmlFor='manufacturer'><b>Manufacturer</b></label>
                        <select name="manufacturer" onChange={this.handleManufacturerChange} className="form-select">
                            <option value=''>Choose a manufacturer</option>
                            {this.state.manufacturers.map(l => <option key={l.id} value={l.id}>{l.name}</option>)}
                        </select>
                </div>
                <div className='d-flex justify-content-center'>
                <button className='btn btn-danger'>Add a Vehicle Model</button>
                </div>
            </form>
        </div>
    )
}
}