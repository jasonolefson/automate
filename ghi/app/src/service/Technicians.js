import React from 'react'
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';




export default function Technicians() {
    const [techs, setTechs] = useState([]);
    const [searchParam, setSearchParam] = useState('');
    useEffect(() => {
        const techListUrl = "http://localhost:8080/api/technicians/";
        fetch(techListUrl)
            .then(resp => resp.json())
            .then(data => setTechs(data.technicians))
    }, [])


    return (
        <div className='container'>
            <div className='d-grid justify-content-center'>
            <div className='fs-2 fw-bold'>Service Technichans:</div>
            <Link className='shadow btn btn-danger' to="/service/technicians/new">Add Technician</Link>
            <div className='mb-3 mt-2 d-flex justify-content-center'>
                <input onChange={e => setSearchParam(e.target.value)} type="text" placeholder="Search Technitian"/>
            </div>
            </div>
            <table className="shadow table table-hover table-striped table-bordered text-center align-middle">
                <thead>
                    <tr>
                        <th scope="col"><b>Name</b></th>
                        <th scope="col"><b>Employee Number:</b></th>
                    </tr>
                </thead>
                <tbody className='table-warning'>
                    {techs.filter(tech => { if (tech.name.toLowerCase().includes(searchParam.toLowerCase())) return tech }).map(tech => <tr key={tech.employee_number}><td>{tech.name}</td><td>{tech.employee_number}</td></tr>)}
                </tbody>
            </table>
        </div>
    )
}
