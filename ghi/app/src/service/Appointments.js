import React from 'react'
import { useState, useEffect } from 'react';
import Appointment from './Appointment';
import { Link } from "react-router-dom";

export default function Appointments() {
    const [appointments, setAppointments] = useState([]);
    const [vipVins, setVipVins] = useState([]);
    const [searchParam, setSearchParam] = useState('');

    useEffect(() => {
        const appsUrl = "http://localhost:8080/api/appointments/";
        fetch(appsUrl)
            .then(res => res.json())
            .then(data => setAppointments(data.appointments))
            .catch(e => console.log('fetching appointments error! :', e));
        const vipVinUrl = 'http://localhost:8080/api/autovos/';
        fetch(vipVinUrl)
            .then(res => res.json())
            .then(data => setVipVins(data.autoVOs))
            .catch(e => console.log("fetching vipVins error", e));
    }, []);

    // fetch get all VIN. map check each a line35 VoArray.includes(a.vin)


    return (
        <div className='container text-center'>
            <div className='fs-2 fw-bold '>Service Appointments:</div>
            <Link to="/service/appointments/new" className='shadow btn btn-danger mt-2 mb-2'>Make an appointment:</Link>
            <div className='my-3'>
                <input onChange={e => setSearchParam(e.target.value.toLowerCase())} type="text" placeholder='Search by VIN' />
            </div>
            <div>
                <table className="shadow table table-hover table-striped table-bordered text-center align-middle">
                    <thead>
                        <tr>
                            <th scope="col">VIN</th>
                            <th scope="col">Customer name</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Technician</th>
                            <th scope="col">Reason</th>
                            <th scope="col">Status</th>
                            <th scope="col">Vip</th>
                        </tr>
                    </thead>
                    <tbody className='table-warning'>
                        {appointments.filter(a => { if (a.vin.toLowerCase().includes(searchParam)) return a }).map(a => <Appointment key={a.id} isVip={vipVins.includes(a.vin) ? true : false} data={a} />)}

                    </tbody>
                </table>
            </div>
        </div>
    )
}
