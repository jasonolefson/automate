from .models import ServiceAppointment, Technician, AutomobileVO
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import TechnicianListEncoder, AppointmentListEncoder

# Create your views here.

@require_http_methods(["GET","POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians":technicians},encoder=TechnicianListEncoder,safe=False)
    else: # POST:
        content = json.loads(request.body)
        # content is an obj
        new_tech = Technician.objects.create(**content)
        return JsonResponse(new_tech,encoder=TechnicianListEncoder,safe=False)


# appointments:
@require_http_methods(["GET","POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse({"appointments":appointments},encoder=AppointmentListEncoder,safe=False)
    else: #POST:
        content = json.loads(request.body)
        try:
            print(content["technician"])
            technician = Technician.objects.get(employee_number=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"meessage":"Invalid employee_number"},status=400)
        new_appointment = ServiceAppointment.objects.create(**content)

        return JsonResponse(new_appointment,encoder=AppointmentListEncoder,safe=False)

@require_http_methods(["PUT"])
def api_update_appointments(request,pk):
    content = json.loads(request.body)
    try:
        print(content["technician"])
        technician = Technician.objects.get(name=content["technician"])
        content["technician"] = technician
    except Technician.DoesNotExist:
        return JsonResponse({"meessage":"Invalid technician name"},status=400)
    ServiceAppointment.objects.filter(id=pk).update(**content)
    new_appointment = ServiceAppointment.objects.get(id=pk)

    return JsonResponse(new_appointment,encoder=AppointmentListEncoder,safe=False)
    # appointment = ServiceAppointment.objects.get(id=pk)

@require_http_methods("PUT")
def api_finish_appointments(request,pk):
    appointment = ServiceAppointment.objects.get(id=pk)
    appointment.finish()
    appointment = ServiceAppointment.objects.get(id=pk)
    return JsonResponse(appointment,encoder=AppointmentListEncoder,safe=False)

@require_http_methods("PUT")
def api_cancel_appointments(request,pk):
    appointment = ServiceAppointment.objects.get(id=pk)
    appointment.cancel()
    appointment = ServiceAppointment.objects.get(id=pk)
    return JsonResponse(appointment,encoder=AppointmentListEncoder,safe=False)


require_http_methods(["GET"])
def api_list_autoVOs(request):
    content = {"autoVOs":[i.vin for i in AutomobileVO.objects.all()]}
    return JsonResponse(content)