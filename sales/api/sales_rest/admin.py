from django.contrib import admin
from .models import AutomobileVO, SalesPerson, PotentialCustomer, Sale, Address
# Register your models here.
admin.site.register(AutomobileVO)
admin.site.register(SalesPerson)
admin.site.register(PotentialCustomer)
admin.site.register(Sale)
admin.site.register(Address)