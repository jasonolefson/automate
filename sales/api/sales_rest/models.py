from django.db import models
from django.urls import reverse
from django.core.validators import MaxValueValidator



# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default=False)


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_no = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse('api_show_sales_person', kwargs={'pk': self.pk})


# Stretch goal: add address model as a foreign key to PotentialCustomer to enable filtering by address in the future and better readability

class Address(models.Model):
    address1 = models.CharField(max_length=120)
    address2 = models.CharField(max_length=120, null=True, blank=True)
    zip_code = models.PositiveIntegerField(validators=[MaxValueValidator(99999)])
    city = models.CharField(max_length=20)
    state = models.CharField(max_length=120)





class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    # address = models.CharField(max_length=300)
    #could use PhoneNumberField with a pip install....
    phone_no = models.CharField(max_length=12)
    customer_address = models.CharField(max_length=200, null=True)

# STRETCH GOAL CODE
    # customer_address = models.ForeignKey(
    #     Address,
    #     related_name="customer",
    #     on_delete=models.CASCADE,
    #     null=True
    # )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse('api_show_potential_customer', kwargs={'pk': self.pk})


class Sale(models.Model):
    sale_price = models.PositiveIntegerField()

# USE PROTECT, AS SALES RECORDS ARE USUALLY KEPT FOR AN EXTENDED PERIOD OF TIME!!!!
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salesrecord",
        on_delete=models.PROTECT
    )

    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="salesrecord",
        on_delete=models.PROTECT
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="salesrecord",
        on_delete=models.PROTECT,
    )